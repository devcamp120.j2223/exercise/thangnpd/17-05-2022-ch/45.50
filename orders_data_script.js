/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gCOLUMN_ORDER_ID = 0;
const gCOLUMN_COMBO_SIZE = 1;
const gCOLUMN_PIZZA_TYPE = 2;
const gCOLUMN_DRINK = 3;
const gCOLUMN_PRICE = 4;
const gCOLUMN_FULLNAME = 5;
const gCOLUMN_PHONE = 6;
const gCOLUMN_STATUS = 7;
const gCOLUMN_ACTION = 8;
var gColName = ['orderId', 'kichCo', 'loaiPizza', 'idLoaiNuocUong', 'thanhTien', 'hoTen', 'soDienThoai', 'trangThai', 'detail'];

const gBASE_URL = "http://42.115.221.44:8080/devcamp-pizza365/orders"; //URL gọi ajax

var gOrdersObjs; // Tạo biến toàn cục chứa orders object array
var gOrdersObjFilter  // Tạo biến toàn cục chứa orders object array sau khi dc filter

// Tạo biến chứa trạng thái
var gStatus = {
  open: "Open",
  confirmed: "Confirmed",
  cancel: "Cancel"
};

// Tạo biến chứa loại pizza
var gPizzaType = {
  hawaii: "Hawaii",
  bacon: "Bacon",
  seafood: "Seafood"
};

//Tạo sẵn bảng bằng Datatable
var gTableOrders = $("#order-table").DataTable({
  // Khai báo các cột của datatable
  columns: [
    { "data": gColName[gCOLUMN_ORDER_ID] },
    { "data": gColName[gCOLUMN_COMBO_SIZE] },
    { "data": gColName[gCOLUMN_PIZZA_TYPE] },
    { "data": gColName[gCOLUMN_DRINK] },
    { "data": gColName[gCOLUMN_PRICE] },
    { "data": gColName[gCOLUMN_FULLNAME] },
    { "data": gColName[gCOLUMN_PHONE] },
    { "data": gColName[gCOLUMN_STATUS] },
    { "data": gColName[gCOLUMN_ACTION] }
  ],
  columnDefs: [
    {
      targets: gCOLUMN_ACTION,
      defaultContent: '<i class="fas fa-info-circle order-detail" title="Chi tiết order" style="cursor:pointer"></i>'
    }
  ]
});

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
function loadOptionFilterStatus() { //Load option vào filter status
  for (var key in gStatus) {
    $('#order-status').append($('<option>', {
      value: key,
      text: gStatus[key]
    }));
  }
}

function loadOptionFilterPizzaType() { // Load option vào filter pizza type
  for (var key in gPizzaType) {
    $('#pizza-type').append($('<option>', {
      value: key,
      text: gPizzaType[key]
    }));
  }
}
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
  callAjacGetAllOrders();
  loadOptionFilterStatus();
  loadOptionFilterPizzaType();
  loadDataToTable(gOrdersObjs);
}

function onBtnFilterOrderClick() {
  console.log("Nút filter order dc nhấn");
  //B1: get data đã 
  var vOrderStatusSelectInput = $("#order-status").val(); //Biến chứa select order status filter
  var vPizzaTypeSelectInput = $("#pizza-type").val(); //Biến chứa select pizza type filter

  //B2: validate data
  var vCheck = validateDataFilter(vOrderStatusSelectInput, vPizzaTypeSelectInput);
  //B3: xử lý data
  if (vCheck < 0) {
    console.log("Không có filter");
    //B4: hiển thị dữ liệu
    loadDataToTable(gOrdersObjs);
  } else {
    if (vCheck > 0) { // filter bằng order status và pizza type
      filterOrderByDataSelect(vOrderStatusSelectInput, vPizzaTypeSelectInput);
      //B4: hiển thị dữ liệu
      loadDataToTable(gOrdersObjFilter);
    }
  }
}

function validateDataFilter(paramOrderStatus, paramPizzaType) {
  var vResult = 1;
  if (paramOrderStatus == 'all' && paramPizzaType == 'all') {
    vResult = -1;
  }
  return vResult;
}

function filterOrderByDataSelect(paramOrderStatus, paramPizzaType) {
  var vResult = gOrdersObjs.filter(function (gOrdersObj) {
    return (gOrdersObj.trangThai == paramOrderStatus || paramOrderStatus == "all") &&
      (gOrdersObj.loaiPizza == paramPizzaType || paramPizzaType == "all");
  })
  console.log(vResult);
  if (vResult.length > 0) { //nếu dữ liệu filter có thì thực hiện việc đổ data vào table sau 
    console.log("Tìm dc " + vResult.length + " order khớp sau khi filter!");
    gOrdersObjFilter = vResult;
  } else { // ko tìm dc user nào hoặc input để rỗng thì render hết data ra lại
    gOrdersObjFilter = gOrdersObjs;
  }
}

function onBtnDetailOrderClick(paramThisRow) {
  console.log("Nút chi tiết được nhấn");
  //Xác định thẻ tr là cha của nút được chọn
  var vThisRow = paramThisRow.closest('tr');
  //Lấy datatable row
  var vDatatableRow = gTableOrders.row(vThisRow);
  //Lấy data của dòng 
  var vOrderData = vDatatableRow.data();

  console.log(vOrderData);
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình */

function callAjacGetAllOrders() {
  $.ajax({
    url: gBASE_URL,
    type: "GET",
    dataType: 'json',
    async: false,
    success: function (responseObject) {
      gOrdersObjs = responseObject;
      console.log(gOrdersObjs);
      loadDataToTable(gOrdersObjs);
    },
    error: function (error) {
      console.assert(error.responseText);
    }
  });
}

function loadDataToTable(paramOrderObj) {
  //Xóa toàn bộ dữ liệu đang có của bảng
  gTableOrders.clear();
  //Cập nhật data cho bảng 
  gTableOrders.rows.add(paramOrderObj);
  //Cập nhật lại giao diện hiển thị bảng
  gTableOrders.draw();
}

$(document).ready(function () {
  onPageLoading();

  //Khi ấn, lấy dữ liệu của Row và ghi vào console
  $("#order-table").on('click', 'td .order-detail', function () {
    onBtnDetailOrderClick(this);
  });

  // Khi ấn nút filter order table
  $("#btn-filter").on('click', onBtnFilterOrderClick);
});